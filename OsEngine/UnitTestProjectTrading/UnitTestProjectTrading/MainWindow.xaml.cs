﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.IO;
using System.Web;
using System.Threading;

// using S#
using Ecng.Common;
using Ecng.Collections;
using Ecng.ComponentModel;
using Ecng.Configuration;
using Ecng.Data;
using Ecng.Data.Providers;
using Ecng.Interop;
using Ecng.Localization;
using Ecng.Net;
using Ecng.Reflection;
using Ecng.Reflection.Aspects;
using Ecng.Security;
using Ecng.Serialization;
using Ecng.Transactions;
using Ecng.UnitTesting;
using Ecng.Web;
using Ecng.Xaml;

using StockSharp.Algo.Storages.Csv;
using StockSharp.Alerts;
using StockSharp.Algo;
using StockSharp.Algo.Indicators;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Candles.Compression;
using StockSharp.Algo.Strategies;
using StockSharp.Algo.Strategies.Reporting;
using StockSharp.Algo.Storages;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Xaml;
using StockSharp.Xaml.Charting;
using StockSharp.Xaml.Diagram;
using StockSharp.Quik;
using StockSharp.Messages;
using StockSharp.Localization;

using System.Diagnostics;
using System.ComponentModel;
using System.Globalization;

using MoreLinq;

using Ookii.Dialogs.Wpf;

namespace BigMoney_EMA
{
    public partial class MainWindow : Window
    {

        public class QuikTraderAdapter : QuikTrader, IConnector
        {
            private Security _security; // инструмент
            private string mySecurity;
            private bool isStrategyRun = false;

            public QuikTraderAdapter(string path, string mySecurity) : base(path)
            {
                this.mySecurity = mySecurity;
            }

            private Trader strategy;
            public void setTraderStrategy(Trader st)
            {
                strategy = st;
            }

            public void setStrategyRunOn()
            {
                isStrategyRun = true;
            }
            public void setStrategyRunOff()
            {
                isStrategyRun = false;
            }
            public bool isStrategyRunOn()
            {
                return isStrategyRun;
            }

            public void initConnection()
            {

                this.Connected += () =>
                {
                    NewSecurity += security =>
                    {
                        if (!security.Code.CompareIgnoreCase(mySecurity))
                        {
                            return;
                        }
                        _security = security;
                    };
           
                };
                this.ConnectionError += ex =>
                {
                    
                };

                this.MarketDepthsChanged += (depths) => {
                    foreach (var depth in depths) {
                        if (isStrategyRun)
                        {
                            var bestBidPrice = depth.BestBid.Price;
                            var bestBidVolume = int.Parse(decimal.Floor(depth.BestBid.Volume).ToString());
                            var bestAskPrice = depth.BestAsk.Price;
                            var bestAskVolume = int.Parse(decimal.Floor(depth.BestAsk.Volume).ToString());
                            var simple = new SimpleMarketDepth(bestBidPrice, bestBidVolume, bestAskPrice, bestAskVolume);
                            strategy.addMarketDepth(simple);
                        }
                    }
                };
            }

            public void marketDepthsListenOn()
            {
                RegisterMarketDepth(_security);
            }
            public void marketDepthsListenOff()
            {
                UnRegisterMarketDepth(_security);
            }

            public decimal getCurrentBalance()
            {
                return 0;
            }

            public int getCountOpenPositions()
            {
                return 0;
            }

            public void openSellPosition(decimal price, decimal stop, decimal take)
            {

            }
            public void closeSellPosition(decimal price)
            {

            }

            public decimal getClosePositionsProfit()
            {
                return 0;
            }

            public decimal getReducerPercent()
            {
                return 0;
            }

            public List<Position> getClosePositions()
            {
                return new List<BigMoney_EMA.Position>();
            }

            public List<Position> getOpenPositions()
            {
                return new List<BigMoney_EMA.Position>();
            }
        }

        private Trader traderStrategy;
        private QuikTrader _trader; 
        private TimeSpan _timeFrame = TimeSpan.FromMinutes(1); 

    

        private string myAccount = "76557ud";

        public bool isStart = false;
        public bool isRealTime;

        public MainWindow()
        {
            InitializeComponent();



            // находим путь к Quik
            quikPath.Text = QuikTerminal.GetDefaultPath();

            StrategyNegativeMolot molotNegative = new StrategyNegativeMolot(0.00185m, 0.013042199m, 0.001m, 1515);
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            traderStrategy = new Trader(molotNegative, rm);
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!isTraderExist())
            {
                if (quikPath.Text.IsEmpty())
                {
                    MessageBox.Show(this, LocalizedStrings.Str2983);
                    return;
                }
                var newConnector = new QuikTraderAdapter(quikPath.Text, "SIZ8");
                traderStrategy.setConnector(newConnector);
                newConnector.setTraderStrategy(traderStrategy);
                newConnector.initConnection();
            }
            QuikTraderAdapter connector = (QuikTraderAdapter)traderStrategy.getConnector();
            if (connector.ConnectionState == ConnectionStates.Disconnected)
            {
                connector.Connect();
            }
            else
            {
                connector.Disconnect();
            }
        }

        private bool isTraderExist()
        {
            return traderStrategy.getConnector() != null;
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            QuikTraderAdapter connector = (QuikTraderAdapter)traderStrategy.getConnector();


            if (!connector.isStrategyRunOn())
            {
                // запускаем процесс получения стакана
                connector.marketDepthsListenOn();
                connector.setStrategyRunOn();
                StartButton.Content = LocalizedStrings.Str242;
            }
            else
            {
                connector.setStrategyRunOff();
                connector.marketDepthsListenOff();
                StartButton.Content = LocalizedStrings.Str2421;
            }
        }

        private void DrawCandle(CandleSeries series, Candle candle)
        {

        }


        private void CheckButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ReportButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void FindPathButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();

            if (!quikPath.Text.IsEmpty())
            {
                dialog.SelectedPath = quikPath.Text;

            }
            if (dialog.ShowDialog(this) == true)
            {
                quikPath.Text = dialog.SelectedPath;
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            QuikTraderAdapter connector = (QuikTraderAdapter)traderStrategy.getConnector();
            if (connector != null)
            {
                connector.Disconnect();
            }

            connector?.Dispose();
            base.OnClosing(e);
        }


        private void OrdersOrderSelected()
        {
            CancelOrdersButton.IsEnabled = !Orders.SelectedOrders.IsEmpty();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (StarCheckBox.IsChecked == true)
            {
                isStart = true;
            }
            else
            {
                isStart = false;
            }
        }

        private void CancelOrdersButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
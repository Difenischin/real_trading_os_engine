﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigMoney_EMA
{
    public class Parser
    {
        public Parser(string nameFile)
        {

        }
        struct Line
        {
            public decimal price;
            public int volume;
            public string direction;
            public string time;
        }
        public static List<SimpleMarketDepth> getMarketDepths(string nameFile)
        {
            List<Line> lines = new List<Line>();
            using (var reader = new System.IO.StreamReader(nameFile))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    var lineStruct = new Line();
                    lineStruct.price = decimal.Parse(values[2]);
                    lineStruct.time = values[0];
                    lineStruct.volume = int.Parse(values[3]);
                    lineStruct.direction = values[4];
                    lines.Add(lineStruct);
                }

            }
            List<SimpleMarketDepth> marketDepths = new List<SimpleMarketDepth>();
            for (int i = 0; i < lines.Count; i += 2)
            {
                if (isMarketDepthValid(lines[i], lines[i + 1]))
                {
                    var a = new SimpleMarketDepth(lines[i].price, lines[i].volume, lines[i + 1].price, lines[i + 1].volume);
                    marketDepths.Add(a);
                }
            }
            return marketDepths;
        }

        private static bool isMarketDepthValid(Line line1, Line line2)
        {
            var isDirectionCorrect = line1.direction == "Buy" && line2.direction == "Sell";
            var isTimeCorrect = line1.time == line2.time;
            var isPriceCorrect = line1.price <= line2.price;
            return isDirectionCorrect && isTimeCorrect && isPriceCorrect;
        }

    }

    public class SimpleMarketDepth
    {
        int askVolume;
        int bidVolume;
        decimal askPrice;
        decimal bidPrice;

        public SimpleMarketDepth(decimal bestBidPrice, int bestBidVolume, decimal bestAskPrice, int bestAskVolume)
        {
            askVolume = bestAskVolume;
            bidVolume = bestBidVolume;
            askPrice = bestAskPrice;
            bidPrice = bestBidPrice;
        }

        public decimal getMiddlePrice()
        {
            return (askPrice + bidPrice) / 2;
        }

        public int getBestAskVolume()
        {
            return askVolume;
        }

        public decimal getAskPrice()
        {
            return askPrice;
        }
    }

    public class StrategyNegativeMolot
    {
        private decimal percentStop;
        private decimal percentProfit;
        private decimal percentRiskReducer;
        private int minCountOrderForStart;

        public StrategyNegativeMolot(decimal percentStop, decimal percentProfit, decimal percentRiskReducer,
            int minCountOrderForStart)
        {
            this.percentStop = percentStop;
            this.percentProfit = percentProfit;
            this.percentRiskReducer = percentRiskReducer;
            this.minCountOrderForStart = minCountOrderForStart;
        }

        public decimal getDesiredPrice(decimal currentPrice)
        {
            var riskAdjustment = 1m + percentRiskReducer;
            var desiredPrice = Decimal.Floor(currentPrice * riskAdjustment);
            if (desiredPrice <= currentPrice)
            {
                return Decimal.Floor(currentPrice + 1);
            }
            return desiredPrice;
        }

        public decimal getStopPrice(decimal currentPrice)
        {
            var step = currentPrice * percentStop;
            var stopPrice = Decimal.Floor(currentPrice + step);
            if (stopPrice <= currentPrice)
            {
                return Decimal.Floor(currentPrice + 1);
            }
            return stopPrice;
        }

        public decimal getProfitPrice(decimal currentPrice)
        {
            var profit = currentPrice * percentProfit;
            var profitPrice = Decimal.Floor(currentPrice - profit);
            if (profitPrice >= currentPrice)
            {
                return Decimal.Floor(currentPrice - 1);
            }
            return profitPrice;
        }

        public bool canOpenSellPosition(SimpleMarketDepth marketDepth)
        {
            int bayerCountOrder = marketDepth.getBestAskVolume();
            return bayerCountOrder > minCountOrderForStart;
        }
    }

    public class DesiredOrder
    {
        private decimal desiredPrice;
        private decimal priceOrder;

        public DesiredOrder(decimal DesiredPrice, decimal priceOrder)
        {
            this.desiredPrice = DesiredPrice;
            this.priceOrder = priceOrder;
        }

        public decimal getDesiredPrice()
        {
            return desiredPrice;
        }

        public decimal getPriceOrder()
        {
            return priceOrder;
        }

        public string ToString()
        {
            string htmlString = string.Format(
                @"
 Желаемая цена <strong>{0}</strong>
 Цена при котором появился нужный объем <strong>{1}</strong>",
                this.desiredPrice,
                this.priceOrder
            );
            return htmlString;
        }
    }

    public class RiskManager
    {
        private int maxCountOpenPositions;
        private decimal percentLesionOnDay;
        private decimal startBalance;

        public RiskManager(int maxCountOpenPositions, decimal percentLesionOnDay, decimal startBalance)
        {
            this.maxCountOpenPositions = maxCountOpenPositions;
            this.percentLesionOnDay = percentLesionOnDay;
            this.startBalance = startBalance;
        }

        public bool canOpenPosition(int countOpenPositions, decimal currentBalance)
        {
            if (countOpenPositions > maxCountOpenPositions)
            {
                return false;
            }

            if (currentBalance < 0)
            {
                return false;
            }

            var desired = currentBalance - startBalance;

            var percentDif = Math.Abs(desired / startBalance);
            if (desired < 0 && percentDif > percentLesionOnDay)
            {
                return false;
            }

            return true;
        }
    }

    public class Position
    {
        private decimal priceOpen;
        private decimal priceClose;
        private decimal priceStop;
        private decimal priceTake;

        public Position(decimal priceOpen, decimal priceStop, decimal priceTake)
        {
            this.priceOpen = priceOpen;
            this.priceStop = priceStop;
            this.priceTake = priceTake;
            priceClose = -5;
        }

        public void close(decimal price)
        {
            priceClose = price;
        }
        public bool isOpen()
        {
            return priceClose == -5;
        }
        public decimal getPriceOpen()
        {
            return priceOpen;
        }
        public decimal getPriceStop()
        {
            return priceStop;
        }
        public decimal getPriceTake()
        {
            return priceTake;
        }

        public decimal getPriceClose()
        {
            return priceClose;
        }
        public Position getClone()
        {
            var clone = new Position(priceOpen, priceStop, priceTake);
            clone.close(priceClose);
            return clone;
        }
    }

    public class Trader
    {
        private StrategyNegativeMolot strategy;
        private RiskManager risk;
        private IConnector connector;

        public Trader(StrategyNegativeMolot strategy, RiskManager risk, IConnector connector = null)
        {
            this.strategy = strategy;
            this.risk = risk;
            this.connector = connector;
        }

        public void setConnector(IConnector connector)
        {
            this.connector = connector;
        }
        public IConnector getConnector()
        {
            return connector;
        }

        private List<DesiredOrder> desiredOrders = new List<DesiredOrder>();

        public void addMarketDepth(SimpleMarketDepth marketDepth)
        {
            if (canOpenSellPosition(marketDepth))
            {
                var currentPrice = marketDepth.getMiddlePrice();
                var price = strategy.getDesiredPrice(currentPrice);
                var bestAskPrice = marketDepth.getAskPrice();
                desiredOrders.Add(new DesiredOrder(price, bestAskPrice));
            }

            if (canOpenPosition())
            {
                openOrdersBy(marketDepth);
            }

            closingForciblyPositions(marketDepth.getMiddlePrice());
        }

        private bool canOpenSellPosition(SimpleMarketDepth marketDepth)
        {
            var strategyOk = strategy.canOpenSellPosition(marketDepth);
            var riskManagerOk = risk.canOpenPosition(connector.getCountOpenPositions(), connector.getCurrentBalance());
            return strategyOk &&
                   !isDesiredOrderExist(marketDepth) &&
                   riskManagerOk;
        }

        private bool isDesiredOrderExist(SimpleMarketDepth marketDepth)
        {
            var orderPrice = marketDepth.getAskPrice();
            foreach (var o in desiredOrders)
            {
                if (Math.Abs(o.getPriceOrder() - orderPrice) <= 0.001m)
                {
                    return true;
                }
            }

            return false;
        }

        public List<DesiredOrder> getDesiredOrders()
        {
            return desiredOrders;
        }

        private bool canOpenPosition()
        {
            int countOpen = connector.getCountOpenPositions();
            decimal balance = connector.getCurrentBalance();
            return risk.canOpenPosition(countOpen, balance);
        }

        private void openOrdersBy(SimpleMarketDepth marketDepth)
        {
            for (var i = 0; i < desiredOrders.Count; i++)
            {
                var currentPrice = marketDepth.getMiddlePrice();
                var desirablePrice = desiredOrders[i].getDesiredPrice();
                if (isGoodPrice(currentPrice, desirablePrice))
                {
                    connector.openSellPosition(Decimal.Floor(currentPrice), strategy.getStopPrice(currentPrice), strategy.getProfitPrice(currentPrice));
                    desiredOrders.RemoveAt(i);
                    i--;
                }
            }
        }
        private bool isGoodPrice(decimal current, decimal desirable)
        {
            return current >= desirable;
        }

        private void closingForciblyPositions(decimal bestBidPrice)
        {
            foreach (var position in connector.getOpenPositions())
            {
                if (isPositionNotCorrect(position, bestBidPrice))
                {
                    connector.closeSellPosition(Decimal.Floor(bestBidPrice));
                }
            }
        }
        private bool isPositionNotCorrect(Position position, decimal bestBidPrice)
        {
            bool priceCreate = position.getPriceOpen() > 1;
            var stopPrice = position.getPriceStop();
            var profitPrice = position.getPriceTake();
            //current 50 stop 100 profit 15
            bool isPriceCorrect = bestBidPrice < stopPrice && bestBidPrice > profitPrice;
            return priceCreate && !isPriceCorrect;
        }

        public List<Position> getOpenPositions()
        {
            return connector.getOpenPositions();
        }

        public List<Position> getClosePositions()
        {
            return connector.getClosePositions();
        }

        public decimal getClosePositionsProfit()
        {
            return connector.getClosePositionsProfit();
        }

        public decimal getOborot()
        {
            decimal oborot = 0;
            foreach (var p in getClosePositions())
            {
                oborot += p.getPriceClose() + p.getPriceOpen();
            }
            return oborot;
        }
        public decimal getClosePositionsProfitCleaner()
        {
            return getClosePositionsProfit() - getOborot() * connector.getReducerPercent();
        }

        public decimal getRecoveryFactor()
        {
            decimal sumProsadka = 1;
            foreach (var p in getClosePositions())
            {
                var isNegativeOrder = p.getPriceOpen() < p.getPriceClose();
                if (isNegativeOrder)
                {
                    sumProsadka += p.getPriceClose() - p.getPriceOpen();
                }
            }
            decimal profit = getClosePositionsProfitCleaner();
            return profit / sumProsadka;
        }

        public decimal getRecoveryFactorSimple()
        {
            List<decimal> loss = new List<decimal>();
            foreach (var p in getClosePositions())
            {
                var isNegativeOrder = p.getPriceOpen() < p.getPriceClose();
                if (isNegativeOrder)
                {
                    loss.Add(p.getPriceClose() - p.getPriceOpen());
                }
            }
            decimal maxLoss = loss.Max();
            decimal profit = getClosePositionsProfitCleaner();
            return profit / maxLoss;
        }


    }

    public class Constraint
    {
        private int volume;
        private int slipage;
        private int hourStart;
        private int hourCloseAllPositions;
        private int hourEnd;
        private bool telegramLoggingMod;
        private bool started;

        public Constraint()
        {
            volume = 1;
            slipage = 5;
            hourStart = 11;
            hourEnd = 22;
            hourCloseAllPositions = 21;
            telegramLoggingMod = false;
            started = true;
        }
        public int getSlipage()
        {
            return slipage;
        }
    }

    public class TestConnector : IConnector
    {
        List<Position> positions = new List<Position>();
        private decimal currentBalance;
        private decimal slipage;
        private decimal reducer;
        public TestConnector(decimal currentBalance, decimal slipage = 10, decimal reducer = 0.0005m)
        {
            this.currentBalance = currentBalance;
            this.slipage = slipage;
            this.reducer = reducer;
        }

        public decimal getCurrentBalance()
        {
            return currentBalance;
        }

        public int getCountOpenPositions()
        {
            return getOpenPositions().Count;
        }

        public void openSellPosition(decimal price, decimal stop, decimal take)
        {
            positions.Add(new Position(price - slipage, stop + slipage, take - slipage));
        }

        public void closeSellPosition(decimal price)
        {
            foreach (var p in positions)
            {
                if (p.isOpen())
                {
                    p.close(price);
                    break;
                }
            }
        }

        public List<Position> getOpenPositions()
        {
            List<Position> open = new List<Position>();
            foreach (var p in positions)
            {
                if (p.isOpen())
                {
                    open.Add(p.getClone());
                }
            }
            return open;
        }

        public List<Position> getClosePositions()
        {
            List<Position> close = new List<Position>();
            foreach (var p in positions)
            {
                if (!p.isOpen())
                {
                    close.Add(p.getClone());
                }
            }
            return close;
        }

        public decimal getClosePositionsProfit()
        {
            var profit = 0m;
            foreach (var close in getClosePositions())
            {
                profit += close.getPriceOpen() - close.getPriceClose();
            }
            return profit;
        }

        public decimal getReducerPercent()
        {
            return reducer;
        }
    }

    public interface IConnector
    {
        decimal getCurrentBalance();

        int getCountOpenPositions();
        List<Position> getOpenPositions();

        List<Position> getClosePositions();

        void openSellPosition(decimal price, decimal stop, decimal take);
        void closeSellPosition(decimal price);

        decimal getClosePositionsProfit();

        decimal getReducerPercent();
      
    }



}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.IO;
using System.ComponentModel;

// using S#
using Ecng.Common;
using Ecng.Collections;
using Ecng.ComponentModel;
using Ecng.Configuration;
using Ecng.Data;
using Ecng.Data.Providers;
using Ecng.Interop;
using Ecng.Localization;
using Ecng.Net;
using Ecng.Reflection;
using Ecng.Reflection.Aspects;
using Ecng.Security;
using Ecng.Serialization;
using Ecng.Transactions;
using Ecng.UnitTesting;
using Ecng.Web;
using Ecng.Xaml;
using System.Web;

using StockSharp.Alerts;
using StockSharp.Algo;
using StockSharp.Algo.Indicators;
using StockSharp.Algo.Candles;
using StockSharp.Algo.Candles.Compression;
using StockSharp.Algo.Strategies;
using StockSharp.Algo.Storages;
using StockSharp.BusinessEntities;
using StockSharp.Logging;
using StockSharp.Xaml;
using StockSharp.Xaml.Charting;
using StockSharp.Xaml.Diagram;
using StockSharp.Quik;
using StockSharp.Messages;
using StockSharp.Localization;

namespace BigMoney_EMA
{
    public class EmaStrategy : Strategy
    {

        private int kTake = 5;
        private int stop = 250;


        private CandleSeries _series;
        private MainWindow _MainWnd;
        private QuikTrader _trader;

        private int index = 0; // свеча на предыдущем шаге (0 - текущая свеча)
        private int indexPred = 1;

        private decimal startPrice = 0;
        private decimal endPrice = 0;

        private Order _buyOrder = null;
        private Order _sellOrder = null;
        private Order _stopTakeOrder = null;

        public ExponentialMovingAverage _fastEma { get; }
        public ExponentialMovingAverage _slowEma { get; }

        
        //private bool _isStart;

        public EmaStrategy(CandleSeries series, QuikTrader trader, MainWindow MainWnd, ExponentialMovingAverage fastEma, ExponentialMovingAverage slowEma)
        {
            _trader = trader;
            _series = series;

            _MainWnd = MainWnd;

            _fastEma = fastEma;
            _slowEma = slowEma;

            // 
           // this.Load();

            // подписываемся на события
            this.Log += OnLog;
            this.PropertyChanged += OnStrategyPropertyChanged;
            //_isStart = isStart;
        }
        private void OnLog(LogMessage message)
        {
            // обрабатываем второстепенные сообщений
            if (message.Level != LogLevels.Info && message.Level != LogLevels.Debug)
            {
                _MainWnd.GuiAsync(() => MessageBox.Show(_MainWnd, message.Message));
            }
        }

        private void OnStrategyPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _MainWnd.GuiAsync(() =>
            {
                _MainWnd.Status.Content = ProcessState;
                _MainWnd.PnL.Content = PnL;
                _MainWnd.Slippage.Content = Slippage;
                _MainWnd.Position.Content = Position;
                _MainWnd.Latency.Content = Latency;
            });
        }

        public void CheckStrategy()
        {
            string checkLine = "";

            if (_buyOrder != null)
            {
                checkLine += " _buyOrder ID: " + _buyOrder.Id.ToString() + " _buyOrder.Price: " + _buyOrder.Price.ToString()
                    + " _buyOrder.State: " + _buyOrder.State.ToString() + " _buyOrder.Time: " + _buyOrder.Time.ToString();
            }
            if (_buyOrder == null)
                checkLine += " _buyOrder == null ";

            if (_stopTakeOrder != null)
            {
                checkLine += " _stopTakeOrder ID: " + _stopTakeOrder.Id.ToString() + " _stopTakeOrder.Price: " + _stopTakeOrder.Price.ToString()
                    + " _stopTakeOrder.State: " + _stopTakeOrder.State.ToString() + " _stopTakeOrder.Time: " + _stopTakeOrder.Time.ToString();
            }
            if (_stopTakeOrder == null)
                checkLine += " _stopTakeOrder == null ";

            if (_sellOrder != null)
            {
                checkLine += " _sellOrder ID: " + _sellOrder.Id.ToString() + " _sellOrder.Price: " + _sellOrder.Price.ToString()
                    + " _sellOrder.State: " + _sellOrder.State.ToString() + " _sellOrder.Time: " + _sellOrder.Time.ToString();
            }
            if (_sellOrder == null)
                checkLine += " _sellOrder == null ";

            checkLine += " startPrice: " + startPrice.ToString()
                + " endPrice: " + endPrice.ToString();

            MessageBox.Show(checkLine);
        }


        protected override void OnStarted()
        {

            // создаем правило для события - завершение каждой свечи
            _trader
                .WhenCandlesFinished(_series)
                .Do(() =>
                {
                    /*if (_trader.GetSessionState(ExchangeBoard.Forts) == SessionStates.ForceStopped
                        || _trader.GetSessionState(ExchangeBoard.Forts) == SessionStates.Ended)
                    {
                        return;
                    }*/
                    //isRealTime = candle.OpenTime + Timeframe > DateTimeOffset.Now || isRealTime;

                   /* if (_MainWnd.isStart == false)
                    {
                        return;
                    }*/
                    if (!_MainWnd.isRealTime)
                    {
                        return;
                    }
                    /*var val1 = _fastEma.GetValue(indexPred);
                    var val2 = _slowEma.GetValue(indexPred);
                    var val3 = _fastEma.GetValue(index);
                    var val4 = _slowEma.GetValue(index);*/

                    if (_fastEma.GetValue(indexPred) == 0 || _slowEma.GetValue(indexPred) == 0
                                                  || _fastEma.GetValue(index) == 0 || _slowEma.GetValue(index) == 0)
                    {
                        return;
                    }

                    #region Логика закрытия позиции

                    #region логика закрытия лонга 

                    if (_buyOrder != null && _stopTakeOrder != null && _sellOrder == null &&
                        _fastEma.GetValue(indexPred) >= _slowEma.GetValue(indexPred) &&
                        _fastEma.GetValue(index) < _slowEma.GetValue(index))
                    {
                        #region создаем правило - ошибка снятия стоп заявки - выход по рынку

                        _stopTakeOrder.WhenCancelFailed(_trader)
                         .Do(() =>
                         {
                             _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                             {
                                 _trader.UnRegisterMarketDepth(this.Security);
                                 //_MainWnd._strategy.Stop();
                                 _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                                 _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "НЕ МОГУ снять стоп-лимит заявку - выход по рынку. Стратегия отключена.");
                                 _MainWnd.LogWindow.ScrollToEnd();
                             }));
                         })
                    .Apply(this);

                        #endregion

                        _trader.CancelOrder(_stopTakeOrder); // отменяем стоп-лимит заявку

                        #region создаем заявку на продажу
                        _sellOrder = new Order
                        {
                            Type = OrderTypes.Market,
                            Portfolio = Portfolio,
                            Price = Security.BestBid.Price,
                            Security = Security,
                            Volume = Volume,
                            Direction = Sides.Sell,
                        };
                        #endregion

                        #region создаем правило - ошибка регистрации заявки на продажу
                        _sellOrder.WhenRegisterFailed(_trader)
                            .Do(() =>
                            {
                                _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                                {
                                    //_MainWnd._strategy.CancelActiveOrders();

                                    _trader.UnRegisterMarketDepth(this.Security);
                                    //_MainWnd._strategy.Stop();
                                    _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                                    _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать ордер на продажу - выход по рынку. Стратегия отключена. Все заявки отменены");
                                    _MainWnd.LogWindow.ScrollToEnd();
                                }));
                            })
                            .Apply(this);
                        #endregion

                        #region создаем правило успешной регистрации заявки на продажу - выход по рынку
                        _sellOrder
                            .WhenMatched(_trader)
                            .Do(() =>
                            {
                                endPrice = 0;

                                // пытаемся и находим последнюю цену сделки на продажу
                                try
                                {
                                    //endPrice = _sellOrder.GetTrades(_trader).FirstOrDefault().Trade.Price;
                                    endPrice = _sellOrder.Price;
                                }
                                catch (Exception)
                                {

                                } while (endPrice == 0) ;

                                _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                                {
                                    _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Выход по рынку: " + DateTime.Now.ToString() + " Цена: " + Convert.ToInt32(endPrice).ToString() + " Профит по сделке: " + Convert.ToInt32(endPrice - startPrice).ToString());
                                    _MainWnd.LogWindow.ScrollToEnd();
                                }));

                                _buyOrder = null;
                                _stopTakeOrder = null;
                                _sellOrder = null;

                                // Переворачиваемся в шорт
                                LogicOpenShort();

                            })
                            .Apply(this);
                        #endregion

                        _trader.RegisterOrder(_sellOrder); // регистрируем заявку на продажу
                    }
                    #endregion

                    #region логика закрытия шорта 

                    if (_buyOrder == null && _stopTakeOrder != null && _sellOrder != null &&
                        _fastEma.GetValue(indexPred) <= _slowEma.GetValue(indexPred) &&
                        _fastEma.GetValue(index) > _slowEma.GetValue(index))
                    {

                        #region создаем правило - ошибка снятия стоп заявки - выход по рынку

                        _stopTakeOrder.WhenCancelFailed(_trader)
                         .Do(() =>
                         {
                             _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                             {
                                 _trader.UnRegisterMarketDepth(this.Security);
                                 //_MainWnd._strategy.Stop();
                                 _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                                 _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "НЕ МОГУ снять стоп-лимит заявку - выход по рынку. Стратегия отключена.");
                                 _MainWnd.LogWindow.ScrollToEnd();
                             }));
                         })
                            .Apply(this);

                        #endregion

                        _trader.CancelOrder(_stopTakeOrder); // отменяем стоп-лимит заявку

                        #region создаем заявку на покупку
                        _buyOrder = new Order
                        {
                            Type = OrderTypes.Market,
                            Portfolio = Portfolio,
                            Price = Security.BestAsk.Price,
                            Security = Security,
                            Volume = Volume,
                            Direction = Sides.Buy,
                        };
                        #endregion

                        #region создаем правило - ошибка регистрации заявки на покупку
                        _buyOrder.WhenRegisterFailed(_trader)
                            .Do(() =>
                            {
                                _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                                {
                                    //_MainWnd._strategy.CancelActiveOrders();

                                    _trader.UnRegisterMarketDepth(this.Security);
                                    //_MainWnd._strategy.Stop();
                                    _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                                    _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать ордер на покупку - выход по рынку. Стратегия отключена. Все заявки отменены");
                                    _MainWnd.LogWindow.ScrollToEnd();
                                }));
                            })
                            .Apply(this);
                        #endregion

                        #region создаем правило успешной регистрации заявки на покупку - выход по рынку
                        _buyOrder
                            .WhenMatched(_trader)
                            .Do(() =>
                            {
                                endPrice = 0;

                                // пытаемся и находим последнюю цену сделки на продажу
                                try
                                {
                                    //endPrice = _buyOrder.GetTrades(_trader).FirstOrDefault().Trade.Price;
                                    endPrice = _buyOrder.Price;
                                }
                                catch (Exception)
                                {

                                } while (endPrice == 0) ;

                                _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                                {
                                    //_MainWnd.profitBox.Text = profit.ToString();
                                    _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Выход по рынку: " + DateTime.Now.ToString() + " Цена: " + Convert.ToInt32(endPrice).ToString() + " Профит по сделке: " + Convert.ToInt32(endPrice - startPrice).ToString());
                                    _MainWnd.LogWindow.ScrollToEnd();
                                }));

                                _buyOrder = null;
                                _stopTakeOrder = null;
                                _sellOrder = null;

                                // Переворачиваемся в лонг
                                LogicOpenLong();

                            })
                            .Apply(this);
                        #endregion

                        _trader.RegisterOrder(_buyOrder); // регистрируем заявку на продажу
                    }
                    #endregion

                    #endregion

                    #region Логика лонга 

                    LogicOpenLong();

                    #endregion

                    #region Логика шорта

                    LogicOpenShort();

                    #endregion
                })
                .Apply(this);

            base.OnStarted();
        }

        public void LogicOpenLong()
        {
            #region если условия входа в лонг есть

            if (_buyOrder == null && _sellOrder == null && _stopTakeOrder == null &&
                _fastEma.GetValue(indexPred) <= _slowEma.GetValue(indexPred) &&
                _fastEma.GetValue(index) > _slowEma.GetValue(index))

            #endregion

            {
                #region создаем заявку на покупку

                _buyOrder = new Order
                {
                    Type = OrderTypes.Market,
                    Price = Security.BestAsk.Price,
                    Portfolio = Portfolio,
                    Security = Security,
                    Volume = Volume,
                    Direction = Sides.Buy,
                };

                #endregion

                #region создаем правило на случай ошибки регистрации заявки

                _buyOrder.WhenRegisterFailed(_trader)
                    .Do(() =>
                    {
                        _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                        {
                            //_MainWnd._strategy.CancelActiveOrders();

                            _trader.UnRegisterMarketDepth(this.Security);
                            //_MainWnd._strategy.Stop();
                            _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                            _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать ордер на покупку. Стратегия отключена. Все заявки отменены");
                            _MainWnd.LogWindow.ScrollToEnd();
                        }));
                    })
                    .Apply(this);

                #endregion

                #region создаем правило на случай когда ордер исполнится

                _buyOrder
                    .WhenMatched(_trader)
                    .Do(() =>
                    {

                        // пытаемся и находим цену последней сделки ордера
                        do
                        {
                            startPrice = 0;
                            try
                            {
                                // startPrice = _buyOrder.GetTrades(_trader).FirstOrDefault().Trade.Price;
                                /* if ((_buyOrder != null) && (_buyOrder.Id == _trader.MyTrades.Last().Order.Id))
                                 {
                                     startPrice = _buyOrder.GetAveragePrice(_trader);
                                 }*/
                                startPrice = _buyOrder.Price;
                            }
                            catch (Exception)
                            {

                            }
                        } while (startPrice == 0);


                        _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                        {
                            _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Заявка на покупку исполнена: " + DateTime.Now.ToString() + " по цене: " + Convert.ToInt32(startPrice).ToString());
                            _MainWnd.LogWindow.ScrollToEnd();
                        }));

                        decimal stopPrice = startPrice - stop; // создаем стоп-цену
                        decimal takePrice = startPrice + kTake * stop; // создаем тейк-цену

                        // создаем стоп-лимит ордер
                        _stopTakeOrder = new Order
                        {
                            Type = OrderTypes.Conditional,
                            Volume = base.Volume,
                            Price = stopPrice,
                            Security = base.Security,
                            Portfolio = base.Portfolio,
                            ExpiryDate = DateTime.MaxValue,
                            Direction = Sides.Sell,
                            Condition = new QuikOrderCondition
                            {
                                Type = QuikOrderConditionTypes.TakeProfitStopLimit,
                                StopPrice = takePrice,
                                StopLimitPrice = stopPrice,
                                Offset = new Unit(0),
                                Spread = new Unit(0),
                            },
                        };

                        #region правило на случай ошибки регистрации стоп-лимит заявки по ордеру
                        _stopTakeOrder.WhenRegisterFailed(_trader)
                        .Do(() =>
                        {
                            _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                            {
                                CancelActiveOrders();

                                _trader.UnRegisterMarketDepth(this.Security);
                                //_MainWnd._strategy.Stop();
                                _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                                _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать стоп-лимит ордер - выход по стопу. Стратегия отключена. Все заявки отменены");
                                _MainWnd.LogWindow.ScrollToEnd();
                            }));
                        })
                        .Apply(this);
                        #endregion

                        #region правило на случай успешной регистрации заявки
                        _stopTakeOrder
                        .WhenRegistered(_trader)
                        .Do(() =>
                        {
                            _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                            {
                                _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Стоп-лимит ордер зарегистрирован: " + DateTime.Now.ToString() + ", стоп-цена: " + (stopPrice).ToString() + ", тейк-цена: " + (takePrice).ToString());
                                _MainWnd.LogWindow.ScrollToEnd();
                            }));
                        }
                    )
                        .Apply(this);
                        #endregion

                        #region правило на случай срабатывания стоп-ордера
                        _stopTakeOrder
                        .WhenMatched(_trader)
                        .Do(() =>
                        {
                            // пытаемся и находим цену срабатывания стоп-ордера
                            endPrice = 0;
                            do
                            {
                                try
                                {
                                    // endPrice = _stopTakeOrder.DerivedOrder.GetTrades(_trader).FirstOrDefault().Trade.Price;
                                    endPrice = _stopTakeOrder.Price;
                                }
                                catch (Exception)
                                {

                                }
                            } while (endPrice == 0);

                            _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                            {
                                // _MainWnd.profitBox.Text = profit.ToString();
                                _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Выход по стоп-лимиту: " + DateTime.Now.ToString() + " Цена: " + Convert.ToInt32(endPrice).ToString() + " Профит по сделке: " + Convert.ToInt32(endPrice - startPrice).ToString());
                                _MainWnd.LogWindow.ScrollToEnd();
                            }));

                            _buyOrder = null;
                            _stopTakeOrder = null;

                        })
                        .Apply(this);
                        #endregion

                        // регистрируем стоп-лимит ордер
                        _trader.RegisterOrder(_stopTakeOrder);

                    })
                    .Apply(this);
                #endregion

                // регистрируем Ордер на покупку
                _trader.RegisterOrder(_buyOrder);
            }
        }

        public void LogicOpenShort()
        {

            #region если условия входа в шорт есть

            if (_buyOrder == null && _sellOrder == null && _stopTakeOrder == null &&
                _fastEma.GetValue(indexPred) >= _slowEma.GetValue(indexPred) &&
                _fastEma.GetValue(index) < _slowEma.GetValue(index))

            #endregion

            {
                #region создаем заявку на продажу

                _sellOrder = new Order
                {
                    Type = OrderTypes.Market,
                    Price = Security.BestBid.Price,
                    Portfolio = Portfolio,
                    Security = Security,
                    Volume = Volume,
                    Direction = Sides.Sell,
                };

                #endregion

                #region создаем правило на случай ошибки регистрации заявки

                _sellOrder.WhenRegisterFailed(_trader)
                    .Do(() =>
                    {
                        _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                        {
                            //_MainWnd._strategy.CancelActiveOrders();

                            _trader.UnRegisterMarketDepth(this.Security);
                            //_MainWnd._strategy.Stop();
                            _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                            _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать ордер на продажу. Стратегия отключена. Все заявки отменены");
                            _MainWnd.LogWindow.ScrollToEnd();
                        }));
                    })
                    .Apply(this);

                #endregion

                #region создаем правило на случай когда ордер исполнится

                _sellOrder
                    .WhenMatched(_trader)
                    .Do(() =>
                    {
                        // пытаемся и находим цену последней сделки ордера
                        do
                        {
                            startPrice = 0;
                            try
                            {
                                /* if ((_sellOrder != null) && (_sellOrder.Id == _trader.MyTrades.Last().Order.Id))
                                 {
                                     startPrice = _sellOrder.GetAveragePrice(_trader);
                                 }*/
                                //startPrice = _sellOrder.GetTrades(_trader).FirstOrDefault().Trade.Price;

                                startPrice = _sellOrder.Price;
                            }
                            catch (Exception)
                            {

                            }
                        } while (startPrice == 0);

                        _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                        {
                            _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Заявка на продажу исполнена: " + DateTime.Now.ToString() + " по цене: " + Convert.ToInt32(startPrice).ToString());
                            _MainWnd.LogWindow.ScrollToEnd();
                        }));

                        decimal stopPrice = startPrice + stop; // создаем стоп-цену
                        decimal takePrice = startPrice - kTake * stop; // создаем тейк-цену

                        // создаем стоп-лимит ордер
                        _stopTakeOrder = new Order
                        {
                            Type = OrderTypes.Conditional,
                            Volume = base.Volume,
                            Price = stopPrice,
                            ExpiryDate = DateTime.MaxValue,
                            Security = base.Security,
                            Portfolio = base.Portfolio,
                            Direction = Sides.Buy,
                            Condition = new QuikOrderCondition
                            {
                                Type = QuikOrderConditionTypes.TakeProfitStopLimit,
                                StopPrice = takePrice,
                                StopLimitPrice = stopPrice,
                                Offset = new Unit(0),
                                Spread = new Unit(0),
                            },
                        };

                        #region правило на случай ошибки регистрации стоп-лимит заявки по ордеру
                        _stopTakeOrder.WhenRegisterFailed(_trader)
                         .Do(() =>
                         {
                             _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                             {
                                 CancelActiveOrders();

                                 _trader.UnRegisterMarketDepth(this.Security);
                                // _MainWnd._strategy.Stop();
                                 _MainWnd.StartButton.Content = LocalizedStrings.Str2421;

                                 _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Не могу зарегистрировать стоп-лимит ордер - выход по стопу. Стратегия отключена. Все заявки отменены");
                                 _MainWnd.LogWindow.ScrollToEnd();
                             }));
                         })
                         .Apply(this);
                        #endregion

                        #region правило на случай успешной регистрации заявки
                        _stopTakeOrder
                        .WhenRegistered(_trader)
                        .Do(() =>
                        {
                            _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                            {
                                _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Стоп-лимит ордер зарегистрирован: " + DateTime.Now.ToString() + ", стоп-цена: " + (stopPrice).ToString() + ", тейк-цена: " + (takePrice).ToString());
                                _MainWnd.LogWindow.ScrollToEnd();
                            }));
                        }
                         )
                        .Apply(this);

                        #endregion

                        #region правило на случай срабатывания стоп-ордера
                        _stopTakeOrder
                        .WhenMatched(_trader)
                        .Do(() =>
                        {
                            // пытаемся и находим цену срабатывания стоп-ордера
                            endPrice = 0;
                            do
                            {
                                try
                                {
                                    //endPrice = _stopTakeOrder.DerivedOrder.GetTrades(_trader).FirstOrDefault().Trade.Price;
                                    endPrice = _stopTakeOrder.Price;
                                }
                                catch (Exception)
                                {

                                }
                            } while (endPrice == 0);


                            _MainWnd.Dispatcher.BeginInvoke(new Action(delegate ()
                            {
                                _MainWnd.LogWindow.AppendText(System.Environment.NewLine + "Выход по стоп-лимиту: " + DateTime.Now.ToString() + " Цена: " + Convert.ToInt32(endPrice).ToString() + " Профит по сделке: " + Convert.ToInt32(endPrice - startPrice).ToString());
                                _MainWnd.LogWindow.ScrollToEnd();
                            }));

                            _sellOrder = null;
                            _stopTakeOrder = null;

                        })
                        .Apply(this);
                        #endregion

                        // регистрируем стоп-лимит ордер
                        _trader.RegisterOrder(_stopTakeOrder);

                    })
                    .Apply(this);
                #endregion

                // регистрируем Ордер на продажу
                _trader.RegisterOrder(_sellOrder);
            }
        }
    }
}
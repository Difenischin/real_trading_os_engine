﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
//using System.IO;


namespace UnitTestProjectTrading
{
    [TestClass]
    public class UnitTest1
    {
        int askVolume = 10;
        int bidVolume = 1500;
        decimal askPrice = 90;
        decimal bidPrice = 110;
        SimpleMarketDepth marketDepth = new SimpleMarketDepth(90, 10, 110, 1500, 12);
        StrategyNegativeMolot molotNegative = new StrategyNegativeMolot(0.00185m, 0.013042199m, 0.001m, 1515);

        [TestMethod]
        public void TestMiddlePrice()
        {
            Assert.AreEqual(100m, marketDepth.getMiddlePrice());
        }


        [TestMethod]
        public void TestGetDesiredPrice()
        {
            Assert.AreEqual(1001, molotNegative.getDesiredPrice(1000));
            Assert.AreEqual(1002, molotNegative.getDesiredPrice(1001));
            Assert.AreEqual(999, molotNegative.getDesiredPrice(998));
            Assert.AreEqual(11, molotNegative.getDesiredPrice(10));
        }

        [TestMethod]
        public void TestGetStopPrice()
        {
            Assert.AreEqual(1001, molotNegative.getStopPrice(1000));
            Assert.AreEqual(11, molotNegative.getStopPrice(10));
        }

        [TestMethod]
        public void TestGetProfitPrice()
        {
            Assert.AreEqual(986, molotNegative.getProfitPrice(1000));
            Assert.AreEqual(9, molotNegative.getProfitPrice(10));
        }

        [TestMethod]
        public void TestGetDesiredOrdersCount()
        {
            Assert.AreEqual(1, getDefaultTraid().getDesiredOrders().Count);
        }

        private Trader getDefaultTraid()
        {
            RiskManager rm = new RiskManager(10, 0.05m, 100);
            var trader = new Trader(molotNegative, rm, new TestConnector(1000, 0));
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 1500, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(106, 10, 110, 800, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(109, 10, 110, 1800, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(108, 10, 110, 1800, hourTrade));//одна позиция открылась
            trader.addMarketDepth(new SimpleMarketDepth(109, 10, 115, 1800, hourTrade));
            return trader;
        }

        [TestMethod]
        public void TestGetOpenPositions()
        {
            var trader = getDefaultTraid();
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            var pesilt = new Position(112, 113, 110);
            Assert.AreEqual(pesilt.getPriceOpen(), trader.getOpenPositions()[0].getPriceOpen());
        }

        [TestMethod]
        public void TestGetClosePositionsCount()
        {
            var trader = getDefaultTraid();
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            Assert.AreEqual(0, trader.getClosePositions().Count);
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200, hourTrade));
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            Assert.AreEqual(1, trader.getClosePositions().Count);
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800, hourTrade));
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200, hourTrade));
            Assert.AreEqual(0, trader.getOpenPositions().Count);
            Assert.AreEqual(2, trader.getClosePositions().Count);
        }
        [TestMethod]
        public void TestGetClosePositionsProfit()
        {
            var trader = getDefaultTraid();
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200, hourTrade));
            Assert.AreEqual(0, trader.getOpenPositions().Count);
            Assert.AreEqual(2, trader.getClosePositions().Count);
            Assert.AreEqual(5, trader.getClosePositionsProfit());
        }

        [TestMethod]
        public void TestOborot()
        {
            var trader = getDefaultTraid();
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200, hourTrade));
            Assert.AreEqual(113 + 112 + 107 + 113, trader.getOborot());
        }

        [TestMethod]
        public void TestGetClosePositionsProfitClean()
        {
            var trader = getDefaultTraid();
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800, hourTrade));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200, hourTrade));
            Assert.AreEqual(5 - 0.2225m, trader.getClosePositionsProfitCleaner());
        }

        [TestMethod]
        public void TestGetRecoveryFactor1()
        {
            var trader = getDefaultTraid();
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(90, 10, 95, 200, hourTrade));// 
            Assert.AreEqual(19.8980m, trader.getRecoveryFactor());
        }
        [TestMethod]
        public void TestGetRecoveryFactor2()
        {
            var trader = getDefaultTraid();
            var hourTrade = 12;
            trader.addMarketDepth(new SimpleMarketDepth(120, 10, 125, 200, hourTrade));// 
            Assert.AreEqual(-0.9197272727272727272727272727m, trader.getRecoveryFactor());
        }

        [TestMethod]
        public void TestGetClosePositionsCountTestConnector()
        {
            var connector = new TestConnector(1000);
            connector.openSellPosition(100, 101, 99);
            connector.openSellPosition(101, 101, 99);
            connector.openSellPosition(102, 101, 99);
            connector.openSellPosition(103, 101, 99);
            connector.closeSellPosition(100);
            connector.closeSellPosition(95);
            Assert.AreEqual(2, connector.getClosePositions().Count);
        }

        [TestMethod]
        public void TestCanOpenSellPosition()
        {
            var hourTrade = 12;
            Assert.AreEqual(false, molotNegative.canOpenSellPosition(new SimpleMarketDepth(90, 10, 110, 1500, hourTrade)));
            Assert.AreEqual(true, molotNegative.canOpenSellPosition(new SimpleMarketDepth(90, 10, 110, 1550, hourTrade)));
        }

        [TestMethod]
        public void TestCanSellPositionOpenRiskManager()
        {
            RiskManager rm = new RiskManager(10, 0.01m, 100);
            Assert.AreEqual(true, rm.canOpenPosition(0, 150));
            Assert.AreEqual(false, rm.canOpenPosition(11, 115));
            Assert.AreEqual(false, rm.canOpenPosition(1, -15));
            Assert.AreEqual(false, rm.canOpenPosition(1, -1));
            Assert.AreEqual(false, rm.canOpenPosition(0, 1));
            Assert.AreEqual(true, rm.canOpenPosition(0, 99));
            Assert.AreEqual(false, rm.canOpenPosition(0, 98));
            Assert.AreEqual(true, rm.canOpenPosition(0, 101));
            Assert.AreEqual(true, rm.canOpenPosition(0, 100));
        }

        [TestMethod]
        public void TestCountOpenPositions()
        {
            var connector = new TestConnector(1000);
            connector.openSellPosition(100, 101, 99);
            connector.openSellPosition(101, 101, 99);
            connector.openSellPosition(102, 101, 99);
            connector.openSellPosition(103, 101, 99);
            Assert.AreEqual(4, connector.getCountOpenPositions());
        }
        [TestMethod]
        public void TestCountOpenPositions1()
        {
            var connector = new TestConnector(1000);
            connector.openSellPosition(100, 101, 99);
            connector.openSellPosition(101, 101, 99);
            connector.openSellPosition(102, 101, 99);
            connector.openSellPosition(103, 101, 99);
            connector.closeSellPosition(100);
            Assert.AreEqual(3, connector.getCountOpenPositions());
        }

        [TestMethod]
        public void TestParserMarketDepths()
        {
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS.csv");
            Assert.AreEqual(102766, marketDepths.Count);
            var first = marketDepths[0];
            Assert.AreEqual(23395, first.getAskPrice());
            Assert.AreEqual(4, first.getBestAskVolume());
            Assert.AreEqual((23395m + 23392m) / 2m, first.getMiddlePrice());
            var who = marketDepths[1];
            Assert.AreEqual(23395, who.getAskPrice());
            Assert.AreEqual(4, who.getBestAskVolume());
            Assert.AreEqual((23392m + 23395m) / 2m, who.getMiddlePrice());
        }

        [TestMethod]
        public void TestTraderMarketDepths()
        {
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(1, 1);
            Assert.AreEqual(14, trader.getClosePositions().Count);
            Assert.AreEqual(0, trader.getOpenPositions().Count);
            Assert.AreEqual(3, trader.getDesiredOrders().Count);
        }
        [TestMethod]
        public void TestTraderMarketDepthsGetProfit()
        {

            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(591982m, trader.getOborot());
            Assert.AreEqual(1224.0090m, trader.getClosePositionsProfitCleaner());
        }
        [TestMethod]
        public void TestTraderMarketDepthsClosePrices()
        {
            /*
             datasets datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_23/quotesS.csv
profit 971.0840000000001  countOrders: 13  recovery  2.284903529411765  stop  0.001  take  0.013042199  countOrderGo  1515  count openOrders  1
openOrders [{'buyPrice': 21232.5, 'stopPrice': 21291.798625, 'takePrice': 20955.4510877425}]
profit closeAllOpenOrder
profit 1055.40425  countOrders: 14  recovery  2.4833041176470587  stop  0.001  take  0.013042199  countOrderGo  1515  count openOrders  0
             
             */
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(21163, trader.getClosePositions()[0].getPriceClose());
            Assert.AreEqual(21103, trader.getClosePositions()[0].getPriceOpen());
            Assert.AreEqual(21162, trader.getClosePositions()[0].getPriceStop());
            Assert.AreEqual(20828, trader.getClosePositions()[0].getPriceTake());

            /*
             closeOrders [{'buyPrice': 21103.5, 'stopPrice': 21162.559974999996, 'takePrice': 20828.1335314135, 'closePrice': 21163.0}, 
             {'buyPrice': 21163.0, 'stopPrice': 21222.170049999997, 'takePrice': 20886.857520572998, 'closePrice': 21222.5}, 
             {'buyPrice': 21175.0, 'stopPrice': 21234.192249999996, 'takePrice': 20898.701014185, 'closePrice': 21236.0}, 
             {'buyPrice': 21184.5, 'stopPrice': 21243.709824999998, 'takePrice': 20908.0771132945, 'closePrice': 21244.0}, 
             {'buyPrice': 21202.5, 'stopPrice': 21261.743124999997, 'takePrice': 20925.842353712498, 'closePrice': 21262.5}, 
             {'buyPrice': 21225.5, 'stopPrice': 21284.785675, 'takePrice': 20948.5423831355, 'closePrice': 21287.5}, 
             {'buyPrice': 21241.5, 'stopPrice': 21300.815274999997, 'takePrice': 20964.3337079515, 'closePrice': 20963.0}, 
             {'buyPrice': 21242.5, 'stopPrice': 21301.817124999998, 'takePrice': 20965.3206657525, 'closePrice': 20963.0}, 
             {'buyPrice': 21239.0, 'stopPrice': 21298.31065, 'takePrice': 20961.866313448998, 'closePrice': 20960.5}, 
             {'buyPrice': 21237.5, 'stopPrice': 21296.807875, 'takePrice': 20960.3858767475, 'closePrice': 20960.0}, 
             {'buyPrice': 21235.5, 'stopPrice': 21294.804174999997, 'takePrice': 20958.4119611455, 'closePrice': 20958.0}, 
             {'buyPrice': 21231.0, 'stopPrice': 21290.29585, 'takePrice': 20953.970651041, 'closePrice': 20952.5}, 
             {'buyPrice': 21058.0, 'stopPrice': 21116.975799999997, 'takePrice': 20783.226951468, 'closePrice': 21120.5}]
openOrders [{'buyPrice': 21232.5, 'stopPrice': 21291.798625, 'takePrice': 20955.4510877425}]
*/
        }
        [TestMethod]
        public void TestGetRecoveryFactor3()
        {
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(591982m, trader.getOborot());
            Assert.AreEqual(1224.0090m, trader.getClosePositionsProfitCleaner());
            var recFactorySimple = trader.getRecoveryFactorSimple();
            Assert.AreEqual(2.8732605633802816901408450704m, trader.getRecoveryFactor());

        }
        [TestMethod]
        public void TestStatistic()
        {
            var testFiles = new List<string> { "quotesS_23.csv", "quotesS_23.csv" };
            var result = "profit = 2448,0180 count = 28 recFact = 2,8732605633802816901408450704 recFactSimple = 38,857428571428571428571428571  countOpenPositions = 0";
            Assert.AreEqual(result, getTotalStatisticString(testFiles));
        }
        [TestMethod]
        public void TestStatisticAll()
        {
            var testFiles = getFilesForTest();
            var result = "profit = 2888,0180 count = 28 recFact = 2,8732605633802816901408450704 recFactSimple = 19,428714285714285714285714286 countOpenPositions = 0";
            Assert.AreEqual(result, getTotalStatisticString(testFiles));
        }

        private string getTotalStatisticString(List<string> fileNames)
        {
            var result = new List<string>();
            var closePositionsProfitCleaner = new List<decimal>();
            var closePositionsCount = new List<int>();
            var openPositionsCount = new List<int>();
            var recoveryFactors = new List<decimal>();
            var recoveryFactorsSimple = new List<decimal>();
            var sumsLoss = new List<decimal>();
            var maxLoss = new List<decimal>();

            foreach (var file in fileNames)
            {
                List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths(file);
                RiskManager rm = new RiskManager(10, 0.05m, 25000);
                StrategyNegativeMolot molotNegative = new StrategyNegativeMolot(0.00185m, 0.013042199m, 0.001m, 1515);
                var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
                foreach (var md in marketDepths)
                {
                    trader.addMarketDepth(md);
                }
                Console.WriteLine(file);
                Console.WriteLine(trader.getStatisticString());
                result.Add(trader.getStatisticString());
                closePositionsProfitCleaner.Add(trader.getClosePositionsProfitCleaner());
                closePositionsCount.Add(trader.getClosePositions().Count);
                openPositionsCount.Add(trader.getOpenPositions().Count);
                recoveryFactors.Add(trader.getRecoveryFactor());
                recoveryFactorsSimple.Add(trader.getRecoveryFactorSimple());
                sumsLoss.Add(trader.getSumLoss());
                maxLoss.Add(trader.getMaxLoss());
            }
            var profit = closePositionsProfitCleaner.Sum();
            return "profit = " + profit +
               " count = " + closePositionsCount.Sum() +
               " recFact = " + (profit / sumsLoss.Sum()) +
               " recFactSimple = " + (profit / maxLoss.Max()) +
               " countOpenPositions = " + openPositionsCount.Sum();
        }

        private List<string> getFilesForTest()
        {
            var part = "C:/Users/Slava/Documents/1рабочие/Genetic/";
            return new List<string>
            {
                        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_23/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_03/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_04/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_05/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_06/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_09/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_10/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_11/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_12/quotesS.csv",

        part + "datasets/FORTS/2018_07_16/quotesS.csv",
        part + "datasets/FORTS/2018_07_17/quotesS.csv",
        part + "datasets/FORTS/2018_07_18/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_19/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_20/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_23/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_24/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_25/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_26/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_27/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_30/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_31/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_01/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_02/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_03/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_06/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_07/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_08/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_09/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_10/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_13/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_14/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_15/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_16/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_17/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_20/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_21/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_22/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_23/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_24/quotesS.csv",

        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_27/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_28/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_29/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_30/quotesS.csv",
        part + "datasets/FORTS/SBRF-9.18_FT@FORTS/2018_08_31/quotesS.csv",
            };

        }

        [TestMethod]
        public void TestConstrainHour()
        {
            var a = new Constraint(11, 20, 21);
            Assert.AreEqual(false, a.isHourValide(0));
            Assert.AreEqual(false, a.isHourValide(10));
            Assert.AreEqual(true, a.isHourValide(11));
            Assert.AreEqual(true, a.isHourValide(20));
            Assert.AreEqual(false, a.isHourValide(21));
            Assert.AreEqual(false, a.isHourValide(22));
        }

        [TestMethod]
        public void TestTraderConstrainHour()
        {
            var a = new Constraint(11, 20, 21);
            Assert.AreEqual(false, true);
        }

    }


    /*
     def getStatisticTotal(results):
+    totalProfit = 0
+    countOrders = 0
+    sumProsadka = 1
+    for t in results:
+        totalProfit += t['profit']
+        countOrders += t['countOrder']
+        if (t['profit'] < 0):
+            sumProsadka += t['profit']
+
+    print('totalProfit ', totalProfit)
+    print('countOrder ', countOrders)
+    print('recovery Factor ', totalProfit / sumProsadka)
*/


    public class Parser
    {
        public Parser(string nameFile)
        {

        }
        struct Line
        {
            public decimal price;
            public int volume;
            public string direction;
            public string time;
        }
        public static List<SimpleMarketDepth> getMarketDepths(string nameFile)
        {
            List<Line> lines = new List<Line>();
            using (var reader = new System.IO.StreamReader(nameFile))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');
                    var lineStruct = new Line();
                    lineStruct.price = decimal.Parse(values[2]);
                    lineStruct.time = values[0];
                    lineStruct.volume = int.Parse(values[3]);
                    lineStruct.direction = values[4];
                    lines.Add(lineStruct);
                }

            }
            List<SimpleMarketDepth> marketDepths = new List<SimpleMarketDepth>();
            for (int i = 0; i < lines.Count; i += 2)
            {
                if (isMarketDepthValid(lines[i], lines[i + 1]))
                {
                    var time = int.Parse(lines[i].time);
                    var a = new SimpleMarketDepth(lines[i].price, lines[i].volume, lines[i + 1].price, lines[i + 1].volume, time);
                    marketDepths.Add(a);
                }
            }
            return marketDepths;
        }

        private static bool isMarketDepthValid(Line line1, Line line2)
        {
            var isDirectionCorrect = line1.direction == "Buy" && line2.direction == "Sell";
            var isTimeCorrect = line1.time == line2.time;
            var isPriceCorrect = line1.price <= line2.price;
            return isDirectionCorrect && isTimeCorrect && isPriceCorrect;
        }

    }

    public class SimpleMarketDepth
    {
        int askVolume;
        int bidVolume;
        decimal askPrice;
        decimal bidPrice;
        int hour;

        public SimpleMarketDepth(decimal bestBidPrice, int bestBidVolume, decimal bestAskPrice, int bestAskVolume, int hour)
        {
            askVolume = bestAskVolume;
            bidVolume = bestBidVolume;
            askPrice = bestAskPrice;
            bidPrice = bestBidPrice;
            this.hour = hour;
        }

        public decimal getMiddlePrice()
        {
            return (askPrice + bidPrice) / 2;
        }

        public int getBestAskVolume()
        {
            return askVolume;
        }

        public decimal getAskPrice()
        {
            return askPrice;
        }
    }

    public class StrategyNegativeMolot
    {
        private decimal percentStop;
        private decimal percentProfit;
        private decimal percentRiskReducer;
        private int minCountOrderForStart;

        public StrategyNegativeMolot(decimal percentStop, decimal percentProfit, decimal percentRiskReducer,
            int minCountOrderForStart)
        {
            this.percentStop = percentStop;
            this.percentProfit = percentProfit;
            this.percentRiskReducer = percentRiskReducer;
            this.minCountOrderForStart = minCountOrderForStart;
        }

        public decimal getDesiredPrice(decimal currentPrice)
        {
            var riskAdjustment = 1m + percentRiskReducer;
            var desiredPrice = Decimal.Floor(currentPrice * riskAdjustment);
            if (desiredPrice <= currentPrice)
            {
                return Decimal.Floor(currentPrice + 1);
            }
            return desiredPrice;
        }

        public decimal getStopPrice(decimal currentPrice)
        {
            var step = currentPrice * percentStop;
            var stopPrice = Decimal.Floor(currentPrice + step);
            if (stopPrice <= currentPrice)
            {
                return Decimal.Floor(currentPrice + 1);
            }
            return stopPrice;
        }

        public decimal getProfitPrice(decimal currentPrice)
        {
            var profit = currentPrice * percentProfit;
            var profitPrice = Decimal.Floor(currentPrice - profit);
            if (profitPrice >= currentPrice)
            {
                return Decimal.Floor(currentPrice - 1);
            }
            return profitPrice;
        }

        public bool canOpenSellPosition(SimpleMarketDepth marketDepth)
        {
            int bayerCountOrder = marketDepth.getBestAskVolume();
            return bayerCountOrder > minCountOrderForStart;
        }
    }

    public class DesiredOrder
    {
        private decimal desiredPrice;
        private decimal priceOrder;

        public DesiredOrder(decimal DesiredPrice, decimal priceOrder)
        {
            this.desiredPrice = DesiredPrice;
            this.priceOrder = priceOrder;
        }

        public decimal getDesiredPrice()
        {
            return desiredPrice;
        }

        public decimal getPriceOrder()
        {
            return priceOrder;
        }

        public string ToString()
        {
            string htmlString = string.Format(
                @"
 Желаемая цена <strong>{0}</strong>
 Цена при котором появился нужный объем <strong>{1}</strong>",
                this.desiredPrice,
                this.priceOrder
            );
            return htmlString;
        }
    }

    public class RiskManager
    {
        private int maxCountOpenPositions;
        private decimal percentLesionOnDay;
        private decimal startBalance;

        public RiskManager(int maxCountOpenPositions, decimal percentLesionOnDay, decimal startBalance)
        {
            this.maxCountOpenPositions = maxCountOpenPositions;
            this.percentLesionOnDay = percentLesionOnDay;
            this.startBalance = startBalance;
        }

        public bool canOpenPosition(int countOpenPositions, decimal currentBalance)
        {
            if (countOpenPositions > maxCountOpenPositions)
            {
                return false;
            }

            if (currentBalance < 0)
            {
                return false;
            }

            var desired = currentBalance - startBalance;

            var percentDif = Math.Abs(desired / startBalance);
            if (desired < 0 && percentDif > percentLesionOnDay)
            {
                return false;
            }

            return true;
        }
    }

    public class Position
    {
        private decimal priceOpen;
        private decimal priceClose;
        private decimal priceStop;
        private decimal priceTake;

        public Position(decimal priceOpen, decimal priceStop, decimal priceTake)
        {
            this.priceOpen = priceOpen;
            this.priceStop = priceStop;
            this.priceTake = priceTake;
            priceClose = -5;
        }

        public void close(decimal price)
        {
            priceClose = price;
        }
        public bool isOpen()
        {
            return priceClose == -5;
        }
        public decimal getPriceOpen()
        {
            return priceOpen;
        }
        public decimal getPriceStop()
        {
            return priceStop;
        }
        public decimal getPriceTake()
        {
            return priceTake;
        }

        public decimal getPriceClose()
        {
            return priceClose;
        }
        public Position getClone()
        {
            var clone = new Position(priceOpen, priceStop, priceTake);
            clone.close(priceClose);
            return clone;
        }
    }

    public class Trader
    {
        private StrategyNegativeMolot strategy;
        private RiskManager risk;
        private IConnector connector;

        public Trader(StrategyNegativeMolot strategy, RiskManager risk, IConnector connector = null)
        {
            this.strategy = strategy;
            this.risk = risk;
            this.connector = connector;
        }

        public void setConnector(IConnector connector)
        {
            this.connector = connector;
        }
        public IConnector getConnector()
        {
            return connector;
        }

        private List<DesiredOrder> desiredOrders = new List<DesiredOrder>();

        public void addMarketDepth(SimpleMarketDepth marketDepth)
        {
            if (canOpenSellPosition(marketDepth))
            {
                var currentPrice = marketDepth.getMiddlePrice();
                var price = strategy.getDesiredPrice(currentPrice);
                var bestAskPrice = marketDepth.getAskPrice();
                desiredOrders.Add(new DesiredOrder(price, bestAskPrice));
            }

            if (canOpenPosition())
            {
                openOrdersBy(marketDepth);
            }

            closingForciblyPositions(marketDepth.getMiddlePrice());
        }

        private bool canOpenSellPosition(SimpleMarketDepth marketDepth)
        {
            var strategyOk = strategy.canOpenSellPosition(marketDepth);
            var riskManagerOk = risk.canOpenPosition(connector.getCountOpenPositions(), connector.getCurrentBalance());
            return strategyOk &&
                   !isDesiredOrderExist(marketDepth) &&
                   riskManagerOk;
        }

        private bool isDesiredOrderExist(SimpleMarketDepth marketDepth)
        {
            var orderPrice = marketDepth.getAskPrice();
            foreach (var o in desiredOrders)
            {
                if (Math.Abs(o.getPriceOrder() - orderPrice) <= 0.001m)
                {
                    return true;
                }
            }

            return false;
        }

        public List<DesiredOrder> getDesiredOrders()
        {
            return desiredOrders;
        }

        private bool canOpenPosition()
        {
            int countOpen = connector.getCountOpenPositions();
            decimal balance = connector.getCurrentBalance();
            return risk.canOpenPosition(countOpen, balance);
        }

        private void openOrdersBy(SimpleMarketDepth marketDepth)
        {
            for (var i = 0; i < desiredOrders.Count; i++)
            {
                var currentPrice = marketDepth.getMiddlePrice();
                var desirablePrice = desiredOrders[i].getDesiredPrice();
                if (isGoodPrice(currentPrice, desirablePrice))
                {
                    connector.openSellPosition(Decimal.Floor(currentPrice), strategy.getStopPrice(currentPrice), strategy.getProfitPrice(currentPrice));
                    desiredOrders.RemoveAt(i);
                    i--;
                }
            }
        }
        private bool isGoodPrice(decimal current, decimal desirable)
        {
            return current >= desirable;
        }

        private void closingForciblyPositions(decimal bestBidPrice)
        {
            foreach (var position in connector.getOpenPositions())
            {
                if (isPositionNotCorrect(position, bestBidPrice))
                {
                    connector.closeSellPosition(Decimal.Floor(bestBidPrice));
                }
            }
        }
        private bool isPositionNotCorrect(Position position, decimal bestBidPrice)
        {
            bool priceCreate = position.getPriceOpen() > 1;
            var stopPrice = position.getPriceStop();
            var profitPrice = position.getPriceTake();
            //current 50 stop 100 profit 15
            bool isPriceCorrect = bestBidPrice < stopPrice && bestBidPrice > profitPrice;
            return priceCreate && !isPriceCorrect;
        }

        public List<Position> getOpenPositions()
        {
            return connector.getOpenPositions();
        }

        public List<Position> getClosePositions()
        {
            return connector.getClosePositions();
        }

        public decimal getClosePositionsProfit()
        {
            return connector.getClosePositionsProfit();
        }

        public decimal getOborot()
        {
            decimal oborot = 0;
            foreach (var p in getClosePositions())
            {
                oborot += p.getPriceClose() + p.getPriceOpen();
            }
            return oborot;
        }
        public decimal getClosePositionsProfitCleaner()
        {
            return getClosePositionsProfit() - getOborot() * connector.getReducerPercent();
        }

        public decimal getRecoveryFactor()
        {
            decimal profit = getClosePositionsProfitCleaner();
            return profit / getSumLoss();
        }

        public decimal getSumLoss()
        {
            decimal sumProsadka = 1;//чтобы небыло деления на 0
            foreach (var p in getClosePositions())
            {
                var isNegativeOrder = p.getPriceOpen() < p.getPriceClose();
                if (isNegativeOrder)
                {
                    sumProsadka += p.getPriceClose() - p.getPriceOpen();
                }
            }
            return sumProsadka;
        }

        public decimal getMaxLoss()
        {
            List<decimal> loss = new List<decimal>() { 1m };//чтобы небыло деления на 0
            foreach (var p in getClosePositions())
            {
                var isNegativeOrder = p.getPriceOpen() < p.getPriceClose();
                if (isNegativeOrder)
                {
                    loss.Add(p.getPriceClose() - p.getPriceOpen());
                }
            }
            return loss.Max();
        }

        public decimal getRecoveryFactorSimple()
        {
            decimal profit = getClosePositionsProfitCleaner();
            return profit / getMaxLoss();
        }

        public string getStatisticString()
        {
            var profit = getClosePositionsProfitCleaner().ToString();
            var countClosePosition = getClosePositions().Count.ToString();
            var recoveryFactor = getRecoveryFactor().ToString();
            var recoverySimple = getRecoveryFactorSimple().ToString();
            var countOpenPositions = getOpenPositions().Count.ToString();
            return "profit = " + profit + 
                " count = " + countClosePosition + 
                " recFact = " + recoveryFactor + 
                " recFactSimple = " + recoverySimple +
                " countOpenPositions = " + countOpenPositions;
        }

    }

    public class Constraint
    {
        private int hourStart;
        private int hourCloseAllPositions;
        private int hourEnd;
        private bool telegramLoggingMod;
        private bool started;

        public Constraint(int hourStart, int hourCloseAllPositions, int hourEnd)
        {
            this.hourStart = hourStart;
            this.hourEnd = hourEnd;
            this.hourCloseAllPositions = hourCloseAllPositions;
            telegramLoggingMod = false;
            started = false;
        }
        public void setHourStart(int hour)
        {
            hourStart = hour;
        }
        public void setHourEnd(int hour)
        {
            hourEnd = hour;
        }
        public void setHourCloseAllPositions(int hour)
        {
            hourCloseAllPositions = hour;
        }
        public void setStartedOn()
        {
            started = true;
        }

        public bool isHourValide(int currentHour)
        {
            if (hourStart <= currentHour && currentHour < hourEnd)
            {
                return true;
            }
            return false;
        }
    }

    public class TestConnector : IConnector
    {
        List<Position> positions = new List<Position>();
        private decimal currentBalance;
        private decimal slipage;
        private decimal reducer;
        public TestConnector(decimal currentBalance, decimal slipage = 10, decimal reducer = 0.0005m)
        {
            this.currentBalance = currentBalance;
            this.slipage = slipage;
            this.reducer = reducer;
        }

        public decimal getCurrentBalance()
        {
            return currentBalance;
        }

        public int getCountOpenPositions()
        {
            return getOpenPositions().Count;
        }

        public void openSellPosition(decimal price, decimal stop, decimal take)
        {
            positions.Add(new Position(price - slipage, stop + slipage, take - slipage));
        }

        public void closeSellPosition(decimal price)
        {
            foreach (var p in positions)
            {
                if (p.isOpen())
                {
                    p.close(price);
                    break;
                }
            }
        }

        public List<Position> getOpenPositions()
        {
            List<Position> open = new List<Position>();
            foreach (var p in positions)
            {
                if (p.isOpen())
                {
                    open.Add(p.getClone());
                }
            }
            return open;
        }

        public List<Position> getClosePositions()
        {
            List<Position> close = new List<Position>();
            foreach (var p in positions)
            {
                if (!p.isOpen())
                {
                    close.Add(p.getClone());
                }
            }
            return close;
        }

        public decimal getClosePositionsProfit()
        {
            var profit = 0m;
            foreach (var close in getClosePositions())
            {
                profit += close.getPriceOpen() - close.getPriceClose();
            }
            return profit;
        }

        public decimal getReducerPercent()
        {
            return reducer;
        }
    }

    public interface IConnector
    {
        decimal getCurrentBalance();

        int getCountOpenPositions();
        List<Position> getOpenPositions();

        List<Position> getClosePositions();

        void openSellPosition(decimal price, decimal stop, decimal take);
        void closeSellPosition(decimal price);

        decimal getClosePositionsProfit();

        decimal getReducerPercent();

    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BigMoney_EMA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigMoney_EMA.Tests
{
    [TestClass]
    public class UnitTest1
    {
        int askVolume = 10;
        int bidVolume = 1500;
        decimal askPrice = 90;
        decimal bidPrice = 110;
        SimpleMarketDepth marketDepth = new SimpleMarketDepth(90, 10, 110, 1500);
        StrategyNegativeMolot molotNegative = new StrategyNegativeMolot(0.00185m, 0.013042199m, 0.001m, 1515);

        [TestMethod]
        public void TestMiddlePrice()
        {
            Assert.AreEqual(100m, marketDepth.getMiddlePrice());
        }


        [TestMethod]
        public void TestGetDesiredPrice()
        {
            Assert.AreEqual(1001, molotNegative.getDesiredPrice(1000));
            Assert.AreEqual(1002, molotNegative.getDesiredPrice(1001));
            Assert.AreEqual(999, molotNegative.getDesiredPrice(998));
            Assert.AreEqual(11, molotNegative.getDesiredPrice(10));
        }

        [TestMethod]
        public void TestGetStopPrice()
        {
            Assert.AreEqual(1001, molotNegative.getStopPrice(1000));
            Assert.AreEqual(11, molotNegative.getStopPrice(10));
        }

        [TestMethod]
        public void TestGetProfitPrice()
        {
            Assert.AreEqual(986, molotNegative.getProfitPrice(1000));
            Assert.AreEqual(9, molotNegative.getProfitPrice(10));
        }

        [TestMethod]
        public void TestGetDesiredOrdersCount()
        {
            Assert.AreEqual(1, getDefaultTraid().getDesiredOrders().Count);
        }

        private Trader getDefaultTraid()
        {
            RiskManager rm = new RiskManager(10, 0.05m, 100);
            var trader = new Trader(molotNegative, rm, new TestConnector(1000, 0));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 1500));
            trader.addMarketDepth(new SimpleMarketDepth(106, 10, 110, 800));
            trader.addMarketDepth(new SimpleMarketDepth(109, 10, 110, 1800));
            trader.addMarketDepth(new SimpleMarketDepth(108, 10, 110, 1800));//одна позиция открылась
            trader.addMarketDepth(new SimpleMarketDepth(109, 10, 115, 1800));
            return trader;
        }

        [TestMethod]
        public void TestGetOpenPositions()
        {
            var trader = getDefaultTraid();
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            var pesilt = new Position(112, 113, 110);
            Assert.AreEqual(pesilt.getPriceOpen(), trader.getOpenPositions()[0].getPriceOpen());
        }

        [TestMethod]
        public void TestGetClosePositionsCount()
        {
            var trader = getDefaultTraid();
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            Assert.AreEqual(0, trader.getClosePositions().Count);
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200));
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            Assert.AreEqual(1, trader.getClosePositions().Count);
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800));
            Assert.AreEqual(1, trader.getOpenPositions().Count);
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200));
            Assert.AreEqual(0, trader.getOpenPositions().Count);
            Assert.AreEqual(2, trader.getClosePositions().Count);
        }
        [TestMethod]
        public void TestGetClosePositionsProfit()
        {
            var trader = getDefaultTraid();
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200));
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200));
            Assert.AreEqual(0, trader.getOpenPositions().Count);
            Assert.AreEqual(2, trader.getClosePositions().Count);
            Assert.AreEqual(5, trader.getClosePositionsProfit());
        }

        [TestMethod]
        public void TestOborot()
        {
            var trader = getDefaultTraid();
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200));
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200));
            Assert.AreEqual(113 + 112 + 107 + 113, trader.getOborot());
        }

        [TestMethod]
        public void TestGetClosePositionsProfitClean()
        {
            var trader = getDefaultTraid();
            trader.addMarketDepth(new SimpleMarketDepth(112, 10, 115, 200));
            trader.addMarketDepth(new SimpleMarketDepth(110, 10, 115, 1800));
            trader.addMarketDepth(new SimpleMarketDepth(105, 10, 110, 200));
            Assert.AreEqual(5 - 0.2225m, trader.getClosePositionsProfitCleaner());
        }

        [TestMethod]
        public void TestGetRecoveryFactor1()
        {
            var trader = getDefaultTraid();
            trader.addMarketDepth(new SimpleMarketDepth(90, 10, 95, 200));// 
            Assert.AreEqual(19.8980m, trader.getRecoveryFactor());
        }
        [TestMethod]
        public void TestGetRecoveryFactor2()
        {
            var trader = getDefaultTraid();
            trader.addMarketDepth(new SimpleMarketDepth(120, 10, 125, 200));// 
            Assert.AreEqual(-0.9197272727272727272727272727m, trader.getRecoveryFactor());
        }

        [TestMethod]
        public void TestGetClosePositionsCountTestConnector()
        {
            var connector = new TestConnector(1000);
            connector.openSellPosition(100, 101, 99);
            connector.openSellPosition(101, 101, 99);
            connector.openSellPosition(102, 101, 99);
            connector.openSellPosition(103, 101, 99);
            connector.closeSellPosition(100);
            connector.closeSellPosition(95);
            Assert.AreEqual(2, connector.getClosePositions().Count);
        }

        [TestMethod]
        public void TestCanOpenSellPosition()
        {
            Assert.AreEqual(false, molotNegative.canOpenSellPosition(new SimpleMarketDepth(90, 10, 110, 1500)));
            Assert.AreEqual(true, molotNegative.canOpenSellPosition(new SimpleMarketDepth(90, 10, 110, 1550)));
        }

        [TestMethod]
        public void TestCanSellPositionOpenRiskManager()
        {
            RiskManager rm = new RiskManager(10, 0.01m, 100);
            Assert.AreEqual(true, rm.canOpenPosition(0, 150));
            Assert.AreEqual(false, rm.canOpenPosition(11, 115));
            Assert.AreEqual(false, rm.canOpenPosition(1, -15));
            Assert.AreEqual(false, rm.canOpenPosition(1, -1));
            Assert.AreEqual(false, rm.canOpenPosition(0, 1));
            Assert.AreEqual(true, rm.canOpenPosition(0, 99));
            Assert.AreEqual(false, rm.canOpenPosition(0, 98));
            Assert.AreEqual(true, rm.canOpenPosition(0, 101));
            Assert.AreEqual(true, rm.canOpenPosition(0, 100));
        }

        [TestMethod]
        public void TestCountOpenPositions()
        {
            var connector = new TestConnector(1000);
            connector.openSellPosition(100, 101, 99);
            connector.openSellPosition(101, 101, 99);
            connector.openSellPosition(102, 101, 99);
            connector.openSellPosition(103, 101, 99);
            Assert.AreEqual(4, connector.getCountOpenPositions());
        }
        [TestMethod]
        public void TestCountOpenPositions1()
        {
            var connector = new TestConnector(1000);
            connector.openSellPosition(100, 101, 99);
            connector.openSellPosition(101, 101, 99);
            connector.openSellPosition(102, 101, 99);
            connector.openSellPosition(103, 101, 99);
            connector.closeSellPosition(100);
            Assert.AreEqual(3, connector.getCountOpenPositions());
        }

        [TestMethod]
        public void TestParserMarketDepths()
        {
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS.csv");
            Assert.AreEqual(102766, marketDepths.Count);
            var first = marketDepths[0];
            Assert.AreEqual(23395, first.getAskPrice());
            Assert.AreEqual(4, first.getBestAskVolume());
            Assert.AreEqual((23395m + 23392m) / 2m, first.getMiddlePrice());
            var who = marketDepths[1];
            Assert.AreEqual(23395, who.getAskPrice());
            Assert.AreEqual(4, who.getBestAskVolume());
            Assert.AreEqual((23392m + 23395m) / 2m, who.getMiddlePrice());
        }

        [TestMethod]
        public void TestTraderMarketDepths()
        {
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(1, 1);
            Assert.AreEqual(14, trader.getClosePositions().Count);
            Assert.AreEqual(0, trader.getOpenPositions().Count);
            Assert.AreEqual(3, trader.getDesiredOrders().Count);
        }
        [TestMethod]
        public void TestTraderMarketDepthsGetProfit()
        {

            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(591982m, trader.getOborot());
            Assert.AreEqual(1224.0090m, trader.getClosePositionsProfitCleaner());
        }
        [TestMethod]
        public void TestTraderMarketDepthsClosePrices()
        {
            /*
             datasets datasets/FORTS/SBRF-9.18_FT@FORTS/2018_07_23/quotesS.csv
profit 971.0840000000001  countOrders: 13  recovery  2.284903529411765  stop  0.001  take  0.013042199  countOrderGo  1515  count openOrders  1
openOrders [{'buyPrice': 21232.5, 'stopPrice': 21291.798625, 'takePrice': 20955.4510877425}]
profit closeAllOpenOrder
profit 1055.40425  countOrders: 14  recovery  2.4833041176470587  stop  0.001  take  0.013042199  countOrderGo  1515  count openOrders  0
             
             */
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(21163, trader.getClosePositions()[0].getPriceClose());
            Assert.AreEqual(21103, trader.getClosePositions()[0].getPriceOpen());
            Assert.AreEqual(21162, trader.getClosePositions()[0].getPriceStop());
            Assert.AreEqual(20828, trader.getClosePositions()[0].getPriceTake());

            /*
             closeOrders [{'buyPrice': 21103.5, 'stopPrice': 21162.559974999996, 'takePrice': 20828.1335314135, 'closePrice': 21163.0}, 
             {'buyPrice': 21163.0, 'stopPrice': 21222.170049999997, 'takePrice': 20886.857520572998, 'closePrice': 21222.5}, 
             {'buyPrice': 21175.0, 'stopPrice': 21234.192249999996, 'takePrice': 20898.701014185, 'closePrice': 21236.0}, 
             {'buyPrice': 21184.5, 'stopPrice': 21243.709824999998, 'takePrice': 20908.0771132945, 'closePrice': 21244.0}, 
             {'buyPrice': 21202.5, 'stopPrice': 21261.743124999997, 'takePrice': 20925.842353712498, 'closePrice': 21262.5}, 
             {'buyPrice': 21225.5, 'stopPrice': 21284.785675, 'takePrice': 20948.5423831355, 'closePrice': 21287.5}, 
             {'buyPrice': 21241.5, 'stopPrice': 21300.815274999997, 'takePrice': 20964.3337079515, 'closePrice': 20963.0}, 
             {'buyPrice': 21242.5, 'stopPrice': 21301.817124999998, 'takePrice': 20965.3206657525, 'closePrice': 20963.0}, 
             {'buyPrice': 21239.0, 'stopPrice': 21298.31065, 'takePrice': 20961.866313448998, 'closePrice': 20960.5}, 
             {'buyPrice': 21237.5, 'stopPrice': 21296.807875, 'takePrice': 20960.3858767475, 'closePrice': 20960.0}, 
             {'buyPrice': 21235.5, 'stopPrice': 21294.804174999997, 'takePrice': 20958.4119611455, 'closePrice': 20958.0}, 
             {'buyPrice': 21231.0, 'stopPrice': 21290.29585, 'takePrice': 20953.970651041, 'closePrice': 20952.5}, 
             {'buyPrice': 21058.0, 'stopPrice': 21116.975799999997, 'takePrice': 20783.226951468, 'closePrice': 21120.5}]
openOrders [{'buyPrice': 21232.5, 'stopPrice': 21291.798625, 'takePrice': 20955.4510877425}]
*/
        }
        [TestMethod]
        public void TestGetRecoveryFactor3()
        {
            List<SimpleMarketDepth> marketDepths = Parser.getMarketDepths("quotesS_23.csv");
            RiskManager rm = new RiskManager(10, 0.05m, 25000);
            var trader = new Trader(molotNegative, rm, new TestConnector(25000, 10));
            foreach (var md in marketDepths)
            {
                trader.addMarketDepth(md);
            }
            Assert.AreEqual(591982m, trader.getOborot());
            Assert.AreEqual(1224.0090m, trader.getClosePositionsProfitCleaner());
            var recFactorySimple = trader.getRecoveryFactorSimple();
            Assert.AreEqual(2.8732605633802816901408450704m, trader.getRecoveryFactor());

        }
    }
}